# Ansible Role - Red Hat CodeReady Containers
This repository contains a role to set up Red Hat CodeReady Containers. This is a simple way to get a OpenShift 4 cluster running on a single machine for development purposes.

## About this role
It can be executed on a remote host or you localhost. It depends on what you add for your inventory.

If it is executed as a normal user, it will install it as that user.
If it is executed as a root user, it will create a 'crc'-user, where it will be set up.

It will require that you have access to the root user through sudo, with no password required.

It requires the host to be RHEL 7 or Fedora (that's what we've tested on).
It requires DNS to be properly configured on the host and that NetworkManager is in use.

## Prerequisites

Currently, we have not automated the fetching of your secret. So you'll need to login to Red Hat's OpenShift Cluster Manager and download your secret manually.

https://cloud.redhat.com/openshift/install/crc/installer-provisioned

Once logged in, press "Download pull Secret" or "Copy Pull Secret"

Place the secret into a folder called files, either in the role or inside the same folder as your playbook is executed.

For example:
```
[user@localhost redhat-codeready-containers]$ find files/ -ls
  8265734      4 drwxr-xr-x   2  user group     4096 Nov 01 21:15 files/
  8258020      4 -rw-r--r--   1  user group     2751 Nov 01 21:15 files/secret.txt
[user@localhost redhat-codeready-containers]$
```

## How to execute this ...


### ... included as a role in your playbook

Usually, you'd want to write a requirements file that will pull down the role so you can use it in your playbooks. There is an example of a requirements file in the example-playbooks directory.

The procedure looks something like this:
```
[user@localhost redhat-codeready-containers]$ cat example-playbook/requirements.yml 
- name: redhat-codeready-containers
  src: https://gitlab.com/redhat/ansible-roles/redhat-codeready-containers.git
  scm: git
[user@localhost redhat-codeready-containers]$ ansible-galaxy install -p ~/.ansible/roles -r example-playbook/requirements.yml
- extracting redhat-codeready-containers to /home/user/.ansible/roles/redhat-codeready-containers
- redhat-codeready-containers was installed successfully
[user@localhost redhat-codeready-containers]$
```

Below is an example of how you can include this role in your playbooks.

```
---
- hosts: codeready-containers
  roles:
    - {role: 'redhat-codeready-containers', tags: 'redhat-codeready-containers'}
```

### ... on its own

You can find an easy to use example in the example-playbooks folder, that you can execute as follows:

```
[user@localhost redhat-codeready-containers]$ cd example-playbook/
[user@localhost example-playbook]$ grep -v ^# inventory/codeready-containers.yml 
[codeready-containers]
localhost ansible_connection=local
[user@localhost example-playbook]$ ansible-playbook install-redhat-codeready-containers.yml 
[user@localhost example-playbook]$
```
